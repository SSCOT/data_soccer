package com.sscot.soccerdataapp

import com.sscot.soccerdataapp.features.player.data.PlayerUseCase
import com.sscot.soccerdataapp.features.team.data.TeamUseCase
import com.sscot.soccerdataapp.features.tournament.data.*
import org.koin.dsl.module

val commonModule = module {
    single { RetrofitClient().retrofit.create(TournamentService::class.java)}
    single { TournamentRepository(get()) }
    single { TournamentUseCase(get()) }

    single { RetrofitClient().retrofit.create(TeamService::class.java)}
    single { TeamRepository(get()) }
    single { TeamUseCase(get()) }

    single { RetrofitClient().retrofit.create(PlayerService::class.java)}
    single { PlayerRepository(get()) }
    single { PlayerUseCase(get()) }
}
