package com.sscot.soccerdataapp

import java.lang.ref.WeakReference

abstract class BasePresenter<T:BaseContract> (contract: T) {
    var view: WeakReference<T>? = null

    init {
        this.view = WeakReference(contract)
    }

    fun getView(): T? {
        return view?.get()
    }
}