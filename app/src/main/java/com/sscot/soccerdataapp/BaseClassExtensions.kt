package com.sscot.soccerdataapp

import java.util.*

open class BaseClassExtensions () {
    fun String.calculateAge(): String? {
        this.let {
            val dateOfBirthArray = it.split("-")

            val dateOfBirthCalendar = Calendar.getInstance()

            dateOfBirthCalendar.set(
                dateOfBirthArray[0].toInt(),
                dateOfBirthArray[1].toInt(),
                dateOfBirthArray[2].toInt()
            )

            val todayCalendar = Calendar.getInstance()

            var ages = todayCalendar.get(Calendar.YEAR) - dateOfBirthCalendar.get(Calendar.YEAR)
            if (todayCalendar.get(Calendar.DAY_OF_YEAR) < dateOfBirthCalendar.get(Calendar.DAY_OF_YEAR))
                ages--

            return Integer.valueOf(ages).toString() + " " + App.getContext()?.resources?.getString(R.string.age_unity)
        }

        return null
    }

    fun String.formatNumberWithDots(): String? {
        val capacityLength = this.length
        val capacityAux = StringBuilder(this)

        if (capacityLength > 3) {
            var count = 1;
            for (i in capacityLength - 1 downTo 0 step 1) {
                if (count == 4) {
                    count = 0
                    capacityAux.insert(i+1, '.')
                }

                count++
            }
        }

        return capacityAux.toString();
    }

    fun Int.calculatePercent(data: Int): Float {
        val percent = (data * 100) / this
        return percent.toFloat()
    }

}