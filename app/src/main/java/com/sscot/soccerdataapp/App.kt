package com.sscot.soccerdataapp

import android.app.Application
import android.content.Context
import com.sscot.soccerdataapp.features.main.mainModule
import com.sscot.soccerdataapp.features.player.playerInfoModule
import com.sscot.soccerdataapp.features.player.playerModule
import com.sscot.soccerdataapp.features.player.playerStandingsModule
import com.sscot.soccerdataapp.features.team.teamModule
import com.sscot.soccerdataapp.features.team.teamRosterModule
import com.sscot.soccerdataapp.features.team.teamStandingsModule
import com.sscot.soccerdataapp.features.tournament.leadersTournamentModule
import com.sscot.soccerdataapp.features.tournament.standingsTournamentModule
import com.sscot.soccerdataapp.features.tournament.tournamentModule
import org.koin.core.context.startKoin
import org.koin.core.module.Module

class App : Application() {
    companion object {
        private lateinit var mContext: Context

        fun getContext(): Context? {
            return mContext
        }
    }

    override fun onCreate() {
        super.onCreate()

        mContext = this

        startKoin {
            modules(
                listOf<Module>(
                    mainModule,
                    commonModule,
                    tournamentModule,
                    standingsTournamentModule,
                    leadersTournamentModule,
                    teamModule,
                    teamRosterModule,
                    teamStandingsModule,
                    playerModule,
                    playerStandingsModule,
                    playerInfoModule
                )
            )
        }
    }

}