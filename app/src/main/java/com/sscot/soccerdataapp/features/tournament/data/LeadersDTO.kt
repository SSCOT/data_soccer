package com.sscot.soccerdataapp.features.tournament.data


import com.google.gson.annotations.SerializedName

data class LeadersDTO(
    @SerializedName("generated_at")
    val generatedAt: String?,
    @SerializedName("schema")
    val schema: String?,
    @SerializedName("season_coverage_info")
    val seasonCoverageInfo: SeasonCoverageInfo?,
    @SerializedName("top_assists")
    val topAssists: List<TopAssist>?,
    @SerializedName("top_cards")
    val topCards: List<TopCard>?,
    @SerializedName("top_goals")
    val topGoals: List<TopGoal>?,
    @SerializedName("top_own_goals")
    val topOwnGoals: List<TopOwnGoal?>?,
    @SerializedName("top_points")
    val topPoints: List<TopPoint?>?,
    @SerializedName("tournament")
    val tournament: Tournament?
) {
    data class SeasonCoverageInfo(
        @SerializedName("max_coverage_level")
        val maxCoverageLevel: String?,
        @SerializedName("max_covered")
        val maxCovered: Int?,
        @SerializedName("min_coverage_level")
        val minCoverageLevel: String?,
        @SerializedName("played")
        val played: Int?,
        @SerializedName("scheduled")
        val scheduled: Int?,
        @SerializedName("season_id")
        val seasonId: String?
    )

    data class TopAssist(
        @SerializedName("assists")
        val assists: Int?,
        @SerializedName("player")
        val player: Player?,
        @SerializedName("rank")
        val rank: Int?,
        @SerializedName("team")
        val team: Team?
    ) {
        data class Player(
            @SerializedName("id")
            val id: String?,
            @SerializedName("name")
            val name: String?
        )

        data class Team(
            @SerializedName("abbreviation")
            val abbreviation: String?,
            @SerializedName("id")
            val id: String?,
            @SerializedName("name")
            val name: String?
        )
    }

    data class TopCard(
        @SerializedName("player")
        val player: Player?,
        @SerializedName("rank")
        val rank: Int?,
        @SerializedName("red_cards")
        val redCards: Int?,
        @SerializedName("team")
        val team: Team?,
        @SerializedName("yellow_cards")
        val yellowCards: Int?,
        @SerializedName("yellow_red_cards")
        val yellowRedCards: Int?
    ) {
        data class Player(
            @SerializedName("id")
            val id: String?,
            @SerializedName("name")
            val name: String?
        )

        data class Team(
            @SerializedName("abbreviation")
            val abbreviation: String?,
            @SerializedName("id")
            val id: String?,
            @SerializedName("name")
            val name: String?
        )
    }

    data class TopGoal(
        @SerializedName("goals")
        val goals: Int?,
        @SerializedName("player")
        val player: Player?,
        @SerializedName("rank")
        val rank: Int?,
        @SerializedName("team")
        val team: Team?
    ) {
        data class Player(
            @SerializedName("id")
            val id: String?,
            @SerializedName("name")
            val name: String?
        )

        data class Team(
            @SerializedName("abbreviation")
            val abbreviation: String?,
            @SerializedName("id")
            val id: String?,
            @SerializedName("name")
            val name: String?
        )
    }

    data class TopOwnGoal(
        @SerializedName("own_goals")
        val ownGoals: Int?,
        @SerializedName("player")
        val player: Player?,
        @SerializedName("rank")
        val rank: Int?,
        @SerializedName("team")
        val team: Team?
    ) {
        data class Player(
            @SerializedName("id")
            val id: String?,
            @SerializedName("name")
            val name: String?
        )

        data class Team(
            @SerializedName("abbreviation")
            val abbreviation: String?,
            @SerializedName("id")
            val id: String?,
            @SerializedName("name")
            val name: String?
        )
    }

    data class TopPoint(
        @SerializedName("assists")
        val assists: Int?,
        @SerializedName("goals")
        val goals: Int?,
        @SerializedName("player")
        val player: Player?,
        @SerializedName("rank")
        val rank: Int?,
        @SerializedName("team")
        val team: Team?
    ) {
        data class Player(
            @SerializedName("id")
            val id: String?,
            @SerializedName("name")
            val name: String?
        )

        data class Team(
            @SerializedName("abbreviation")
            val abbreviation: String?,
            @SerializedName("id")
            val id: String?,
            @SerializedName("name")
            val name: String?
        )
    }

    data class Tournament(
        @SerializedName("category")
        val category: Category?,
        @SerializedName("current_season")
        val currentSeason: CurrentSeason?,
        @SerializedName("id")
        val id: String?,
        @SerializedName("name")
        val name: String?,
        @SerializedName("sport")
        val sport: Sport?
    ) {
        data class Category(
            @SerializedName("country_code")
            val countryCode: String?,
            @SerializedName("id")
            val id: String?,
            @SerializedName("name")
            val name: String?
        )

        data class CurrentSeason(
            @SerializedName("end_date")
            val endDate: String?,
            @SerializedName("id")
            val id: String?,
            @SerializedName("name")
            val name: String?,
            @SerializedName("start_date")
            val startDate: String?,
            @SerializedName("year")
            val year: String?
        )

        data class Sport(
            @SerializedName("id")
            val id: String?,
            @SerializedName("name")
            val name: String?
        )
    }
}