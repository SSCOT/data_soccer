package com.sscot.soccerdataapp.features.player

import PlayerStandingsFragment
import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import androidx.viewpager.widget.ViewPager
import com.google.android.material.tabs.TabLayout
import com.sscot.soccerdataapp.BaseCallback
import com.sscot.soccerdataapp.BaseContract
import com.sscot.soccerdataapp.BasePresenter
import com.sscot.soccerdataapp.R
import com.sscot.soccerdataapp.features.player.data.PlayerModel
import com.sscot.soccerdataapp.features.player.data.PlayerUseCase
import kotlinx.android.synthetic.main.activity_team.*
import org.koin.android.ext.android.inject
import org.koin.core.parameter.parametersOf

private const val NUM_PAGES: Int = 2
private var currentPlayer: String? = null
private var mTabLayout: TabLayout? = null
private var mViewPager: ViewPager? = null

class PlayerActivity : AppCompatActivity(), PlayerContract {

    private val presenter: PlayerPresenter by inject { parametersOf(this) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        currentPlayer = intent?.extras?.getString("idPlayer")
        setContentView(R.layout.activity_player)

        currentPlayer?.let { presenter.fragmentReady(it) }

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    override fun configureTitle(title: String) {
        setTitle(title)
    }

    override fun configureTabLayout() {
        mTabLayout = tabLayout
        mTabLayout?.let {
            it.addTab(it.newTab().setText(getString(R.string.player_tab_info)))
            it.addTab(it.newTab().setText(getString(R.string.player_tab_standings)))
            it.tabGravity = TabLayout.GRAVITY_FILL
        }
    }

    override fun configureViewPager(playerModel: PlayerModel) {
        val tournamentPagerAdapter = PlayerPagerAdapter(supportFragmentManager, playerModel)
        mViewPager = viewPager
        mViewPager?.let {

            it.adapter = tournamentPagerAdapter
            it.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(mTabLayout))

            mTabLayout?.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
                override fun onTabSelected(tab: TabLayout.Tab) {
                    val pos = tab.position
                    it.setCurrentItem(pos)
                }

                override fun onTabUnselected(tab: TabLayout.Tab) {}
                override fun onTabReselected(tab: TabLayout.Tab) {}
            })
        }
    }

    override fun loading(loading: Boolean, info: String?) {

        // TODO implementar loading en el diseño
        // txt_loading.text = resources.getString(R.string.loading_team)

        when (loading) {
            true -> {
                // TODO implementar
                /*rotate_loading.start()
                view_loading.visibility = View.VISIBLE*/
            }
            false -> {
                // TODO implementar
                /*rotate_loading.stop()
                view_loading.visibility = View.INVISIBLE*/
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
            else -> return super.onOptionsItemSelected(item);

        }
    }
}

class PlayerPresenter(private val playerUseCase: PlayerUseCase, contract: PlayerContract) :
    BasePresenter<PlayerContract>(contract) {
    fun fragmentReady(idPlayer: String) {
        getData(idPlayer)
    }

    private fun getData(idPlayer: String) {
        //getView()?.loading(true, null)
        playerUseCase.getPlayer(idPlayer, object : BaseCallback<PlayerModel>() {
            override fun onError(codeError: Int?, stringError: String?, exceptionError: Throwable?) {
                super.onError(codeError, stringError, exceptionError)
            }

            override fun onResponse(dataResponse: PlayerModel) {
                super.onResponse(dataResponse)
                getView()?.configureTitle(dataResponse.getCompleteName())
                getView()?.configureTabLayout()
                //getView()?.configureViewPager(dataResponse)
                //getView()?.loading(false, null)
            }
        })
    }
}

interface PlayerContract : BaseContract {
    fun configureTitle(title: String)
    fun configureTabLayout()
    fun configureViewPager(dataResponse: PlayerModel)
}

class PlayerPagerAdapter(fm: FragmentManager, private val playerModel: PlayerModel) : FragmentStatePagerAdapter(fm) {
    override fun getItem(position: Int): Fragment? {
        var fragment: Fragment? = null
        val args = Bundle()

        when (position) {
            0 -> {
                val playerInfoFragment = PlayerInfoFragment()
                args.putSerializable("playerModel", playerModel)
                playerInfoFragment.arguments = args
                fragment = playerInfoFragment

            }
            1 -> {
                val playerStandingsFragment = PlayerStandingsFragment()
                args.putSerializable("playerModel", playerModel)
                playerStandingsFragment.arguments = args
                fragment = playerStandingsFragment
            }
        }

        return fragment
    }

    override fun getCount(): Int {
        return NUM_PAGES
    }

}
