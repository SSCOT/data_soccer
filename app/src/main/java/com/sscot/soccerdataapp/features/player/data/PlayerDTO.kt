package com.sscot.soccerdataapp.features.player.data


import com.google.gson.annotations.SerializedName

data class PlayerDTO(
    @SerializedName("generated_at")
    val generatedAt: String?,
    @SerializedName("player")
    val player: Player?,
    @SerializedName("roles")
    val roles: List<Role?>?,
    @SerializedName("schema")
    val schema: String?,
    @SerializedName("statistics")
    val statistics: Statistics?,
    @SerializedName("teams")
    val teams: List<Team?>?
) {
    data class Player(
        @SerializedName("country_code")
        val countryCode: String?,
        @SerializedName("date_of_birth")
        val dateOfBirth: String?,
        @SerializedName("first_name")
        val firstName: String?,
        @SerializedName("gender")
        val gender: String?,
        @SerializedName("height")
        val height: Int?,
        @SerializedName("id")
        val id: String?,
        @SerializedName("last_name")
        val lastName: String?,
        @SerializedName("name")
        val name: String?,
        @SerializedName("nationality")
        val nationality: String?,
        @SerializedName("preferred_foot")
        val preferredFoot: String?,
        @SerializedName("type")
        val type: String?,
        @SerializedName("weight")
        val weight: Int?
    )

    data class Role(
        @SerializedName("active")
        val active: String?,
        @SerializedName("end_date")
        val endDate: String?,
        @SerializedName("jersey_number")
        val jerseyNumber: Int?,
        @SerializedName("start_date")
        val startDate: String?,
        @SerializedName("team")
        val team: Team?,
        @SerializedName("type")
        val type: String?
    ) {
        data class Team(
            @SerializedName("abbreviation")
            val abbreviation: String?,
            @SerializedName("country")
            val country: String?,
            @SerializedName("country_code")
            val countryCode: String?,
            @SerializedName("id")
            val id: String?,
            @SerializedName("name")
            val name: String?
        )
    }

    data class Statistics(
        @SerializedName("seasons")
        val seasons: List<Season?>?,
        @SerializedName("totals")
        val totals: Totals?
    ) {
        data class Season(
            @SerializedName("id")
            val id: String?,
            @SerializedName("name")
            val name: String?,
            @SerializedName("season_coverage_info")
            val seasonCoverageInfo: SeasonCoverageInfo?,
            @SerializedName("statistics")
            val statistics: Statistics?,
            @SerializedName("team")
            val team: Team?,
            @SerializedName("tournament")
            val tournament: Tournament?
        ) {
            data class SeasonCoverageInfo(
                @SerializedName("max_coverage_level")
                val maxCoverageLevel: String?,
                @SerializedName("max_covered")
                val maxCovered: Int?,
                @SerializedName("min_coverage_level")
                val minCoverageLevel: String?,
                @SerializedName("played")
                val played: Int?,
                @SerializedName("scheduled")
                val scheduled: Int?,
                @SerializedName("season_id")
                val seasonId: String?
            )

            data class Statistics(
                @SerializedName("assists")
                val assists: Int?,
                @SerializedName("goals_scored")
                val goalsScored: Int?,
                @SerializedName("last_event_time")
                val lastEventTime: String?,
                @SerializedName("matches_played")
                val matchesPlayed: Int?,
                @SerializedName("own_goals")
                val ownGoals: Int?,
                @SerializedName("red_cards")
                val redCards: Int?,
                @SerializedName("substituted_in")
                val substitutedIn: Int?,
                @SerializedName("substituted_out")
                val substitutedOut: Int?,
                @SerializedName("yellow_cards")
                val yellowCards: Int?,
                @SerializedName("yellow_red_cards")
                val yellowRedCards: Int?
            )

            data class Team(
                @SerializedName("abbreviation")
                val abbreviation: String?,
                @SerializedName("country")
                val country: String?,
                @SerializedName("country_code")
                val countryCode: String?,
                @SerializedName("id")
                val id: String?,
                @SerializedName("name")
                val name: String?
            )

            data class Tournament(
                @SerializedName("category")
                val category: Category?,
                @SerializedName("id")
                val id: String?,
                @SerializedName("name")
                val name: String?,
                @SerializedName("sport")
                val sport: Sport?
            ) {
                data class Category(
                    @SerializedName("country_code")
                    val countryCode: String?,
                    @SerializedName("id")
                    val id: String?,
                    @SerializedName("name")
                    val name: String?
                )

                data class Sport(
                    @SerializedName("id")
                    val id: String?,
                    @SerializedName("name")
                    val name: String?
                )
            }
        }

        data class Totals(
            @SerializedName("assists")
            val assists: Int?,
            @SerializedName("goals_scored")
            val goalsScored: Int?,
            @SerializedName("last_event_time")
            val lastEventTime: String?,
            @SerializedName("matches_played")
            val matchesPlayed: Int?,
            @SerializedName("own_goals")
            val ownGoals: Int?,
            @SerializedName("red_cards")
            val redCards: Int?,
            @SerializedName("substituted_in")
            val substitutedIn: Int?,
            @SerializedName("substituted_out")
            val substitutedOut: Int?,
            @SerializedName("yellow_cards")
            val yellowCards: Int?,
            @SerializedName("yellow_red_cards")
            val yellowRedCards: Int?
        )
    }

    data class Team(
        @SerializedName("abbreviation")
        val abbreviation: String?,
        @SerializedName("country")
        val country: String?,
        @SerializedName("country_code")
        val countryCode: String?,
        @SerializedName("id")
        val id: String?,
        @SerializedName("name")
        val name: String?
    )
}