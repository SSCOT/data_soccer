package com.sscot.soccerdataapp.features.tournament

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import androidx.viewpager.widget.ViewPager
import com.google.android.material.tabs.TabLayout
import com.sscot.soccerdataapp.*
import com.sscot.soccerdataapp.features.tournament.data.LeadersModel
import com.sscot.soccerdataapp.features.tournament.data.StandingsModel
import com.sscot.soccerdataapp.features.tournament.data.TournamentUseCase
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_tournament.*
import org.koin.android.ext.android.inject
import org.koin.core.parameter.parametersOf
import java.io.Serializable

private const val NUM_PAGES: Int = 2
private const val LOADING_STANDINGS: String = "standings"
private const val LOADING_LEADERS: String = "leaders"
private var currentTournament: String = BuildConfig.ID_LALIGA

class TournamentFragment : Fragment(), TournamentContract {

    lateinit var mContext: FragmentActivity
    lateinit var mTabLayout: TabLayout
    lateinit var mViewPager: ViewPager

    private val presenter: TournamentPresenter by inject { parametersOf(this) }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.activity_tournament, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val arguments = this.arguments
        arguments?.let {
            currentTournament = it.getString("idTournament", BuildConfig.ID_LALIGA)
            activity?.setTitle(it.getString("nameTournament", "Soccer App Data"))
        }

        this.mTabLayout = tabLayout as TabLayout
        this.mViewPager = viewPager as ViewPager
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        presenter.fragmentCreated(currentTournament)
    }

    override fun onAttach(context: Context?) {
        context?.let {
            val context_ini = it
            mContext = context_ini as FragmentActivity

            super.onAttach(it)
        }
    }

    override fun configureStyle() {
        context?.let {

            var colorPrimary: Int = 0
            var colorPrimaryDark: Int = 0

            when (currentTournament) {
                BuildConfig.ID_LALIGA -> {
                    colorPrimary = ContextCompat.getColor(it, R.color.colorPrimaryLaLiga)
                    colorPrimaryDark = ContextCompat.getColor(it, R.color.colorPrimaryDarkLaLiga)
                }

                BuildConfig.ID_PREMIER -> {
                    colorPrimary = ContextCompat.getColor(it, R.color.colorPrimaryPremiere)
                    colorPrimaryDark = ContextCompat.getColor(it, R.color.colorPrimaryDarkPremiere)
                }

                BuildConfig.ID_BUNDESLIGA -> {
                    colorPrimary = ContextCompat.getColor(it, R.color.colorPrimaryBundesliga)
                    colorPrimaryDark =
                        ContextCompat.getColor(it, R.color.colorPrimaryDarkBundesliga)
                }

                BuildConfig.ID_LIGUE1 -> {
                    colorPrimary = ContextCompat.getColor(it, R.color.colorPrimaryLigue1)
                    colorPrimaryDark = ContextCompat.getColor(it, R.color.colorPrimaryDarkLigue1)
                }

                BuildConfig.ID_SERIEA -> {
                    colorPrimary = ContextCompat.getColor(it, R.color.colorPrimarySerieA)
                    colorPrimaryDark = ContextCompat.getColor(it, R.color.colorPrimaryDarkSerieA)
                }
            }

            activity?.let {
                it.window?.statusBarColor = colorPrimaryDark
                it.toolbar.setBackgroundColor(colorPrimary)
            }

            mTabLayout.setBackgroundColor(colorPrimaryDark)
            mTabLayout.setTabTextColors(colorPrimary, ContextCompat.getColor(it, R.color.white))
        }
    }

    override fun configureTabLayout() {
        mTabLayout.addTab(mTabLayout.newTab().setText(getString(R.string.tournament_tab_standings)))
        mTabLayout.addTab(mTabLayout.newTab().setText(getString(R.string.tournament_tab_leaders)))
        mTabLayout.tabGravity = TabLayout.GRAVITY_FILL
    }

    override fun configureViewPager(standingsDataResponse: List<StandingsModel>, leadersDataRespose: LeadersModel) {
        mContext.let {
            val tournamentPagerAdapter = TournamentPagerAdapter(it.supportFragmentManager, standingsDataResponse, leadersDataRespose)
            mViewPager.adapter = tournamentPagerAdapter
            mViewPager.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(mTabLayout))
            mTabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
                override fun onTabSelected(tab: TabLayout.Tab) {
                    val pos = tab.position
                    mViewPager.setCurrentItem(pos)
                }

                override fun onTabUnselected(tab: TabLayout.Tab) {}
                override fun onTabReselected(tab: TabLayout.Tab) {}
            })
        }
    }

    override fun loading(loading: Boolean, info: String?) {

        var infoText: String = ""

        info?.let {
            when (info) {
                LOADING_STANDINGS -> infoText = resources.getString(R.string.loading_standings)
                LOADING_LEADERS -> infoText = resources.getString(R.string.loading_leaders)
            }
        }

        txt_loading.text = infoText

        when (loading) {
            true -> {
                rotate_loading.start()
                view_loading.visibility = View.VISIBLE
            }
            false -> {
                rotate_loading.stop()
                view_loading.visibility = View.GONE
            }
        }
    }
}

class TournamentPresenter(private val tournamentUseCase: TournamentUseCase, contract: TournamentContract) : BasePresenter<TournamentContract>(contract) {

    fun fragmentCreated(idTournament: String) {
        getView()?.configureStyle()
        getStandingsData(idTournament)
    }

    private fun getStandingsData(idTournament: String) {
        getView()?.loading(true, LOADING_STANDINGS)
        tournamentUseCase.getStandings(
            idTournament,
            object : BaseCallback<List<StandingsModel>>() {
                override fun onError(
                    codeError: Int?,
                    stringError: String?,
                    exceptionError: Throwable?
                ) {
                    getView()?.loading(false, null)
                    super.onError(codeError, stringError, exceptionError)
                }

                override fun onResponse(standingsDataResponse: List<StandingsModel>) {
                    super.onResponse(standingsDataResponse)

                    getLeadersData(standingsDataResponse, idTournament)
                }

            })
    }

    private fun getLeadersData(standingsDataResponse: List<StandingsModel>, idTournament: String) {

        // La api no permite llamadas tan rápido así que metemos un delay. Versión de prueba
        getView()?.loading(true, LOADING_LEADERS)
        Thread.sleep(1000)

        tournamentUseCase.getLeaders(idTournament, object : BaseCallback<LeadersModel>() {
            override fun onError(codeError: Int?, stringError: String?, exceptionError: Throwable?) {
                super.onError(codeError, stringError, exceptionError)
                getView()?.loading(false, null)
            }

            override fun onResponse(leadersDataResponse: LeadersModel) {
                super.onResponse(leadersDataResponse)
                getView()?.configureTabLayout()
                getView()?.configureViewPager(standingsDataResponse, leadersDataResponse)
                getView()?.loading(false, null)
            }
        })
    }
}

interface TournamentContract : BaseContract {
    fun configureTabLayout()
    fun configureViewPager(standingsDataResponse: List<StandingsModel>, leadersDataRespose: LeadersModel)
    fun configureStyle()
}

class TournamentPagerAdapter(fm: FragmentManager, private val standingsModel: List<StandingsModel>, private val leadersModel: LeadersModel) : FragmentStatePagerAdapter(fm) {
    override fun getItem(position: Int): Fragment? {
        var fragment: Fragment? = null
        val args = Bundle()

        when (position) {
            0 -> {
                val standingsTournamentFragment = StandingsTournamentFragment()
                args.putSerializable("standigsModel", standingsModel as Serializable)
                standingsTournamentFragment.arguments = args
                fragment = standingsTournamentFragment
            }
            1 -> {
                val leadersTournamentFragment = LeadersTournamentFragment()
                args.putSerializable("leadersModel", leadersModel)
                leadersTournamentFragment.arguments = args
                fragment = leadersTournamentFragment
            }
        }

        return fragment
    }

    override fun getCount(): Int {
        return NUM_PAGES
    }

}