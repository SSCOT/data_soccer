package com.sscot.soccerdataapp.features.team

import TeamStandingsFragment
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import androidx.viewpager.widget.ViewPager
import com.google.android.material.tabs.TabLayout
import com.sscot.soccerdataapp.BaseCallback
import com.sscot.soccerdataapp.BaseContract
import com.sscot.soccerdataapp.BasePresenter
import com.sscot.soccerdataapp.R
import com.sscot.soccerdataapp.features.team.data.TeamModel
import com.sscot.soccerdataapp.features.team.data.TeamUseCase
import kotlinx.android.synthetic.main.activity_team.*
import org.koin.android.ext.android.inject
import org.koin.core.parameter.parametersOf

private const val NUM_PAGES: Int = 2
private var currentTeam: String? = null
private lateinit var mTabLayout: TabLayout
private lateinit var mViewPager: ViewPager

class TeamActivity : AppCompatActivity(), TeamContract {

    private val presenter: TeamPresenter by inject { parametersOf(this) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        currentTeam = intent?.extras?.getString("idTeam")
        setContentView(R.layout.activity_team)

        currentTeam?.let { presenter.fragmentReady(it) }

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    override fun configureTitle(title: String) {
        setTitle(title)
    }

    override fun configureTabLayout() {
        mTabLayout = tabLayout
        mTabLayout.addTab(mTabLayout.newTab().setText(getString(R.string.team_tab_standings)))
        mTabLayout.addTab(mTabLayout.newTab().setText(getString(R.string.team_tab_schedule)))
        mTabLayout.tabGravity = TabLayout.GRAVITY_FILL
    }

    override fun configureViewPager(teamModel: TeamModel) {
        mViewPager = viewPager
        val tournamentPagerAdapter = TeamPagerAdapter(supportFragmentManager, teamModel)
        mViewPager.adapter = tournamentPagerAdapter
        mViewPager.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(mTabLayout))
        mTabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                val pos = tab.position
                mViewPager.setCurrentItem(pos)
            }

            override fun onTabUnselected(tab: TabLayout.Tab) {}
            override fun onTabReselected(tab: TabLayout.Tab) {}
        })

    }

    override fun loading(loading: Boolean, info: String?) {

        txt_loading.text = resources.getString(R.string.loading_team)

        when (loading) {
            true -> {
                rotate_loading.start()
                view_loading.visibility = View.VISIBLE
            }
            false -> {
                rotate_loading.stop()
                view_loading.visibility = View.INVISIBLE
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
            else -> return super.onOptionsItemSelected(item);

        }
    }
}

class TeamPresenter(private val teamUseCase: TeamUseCase, contract: TeamContract) :
    BasePresenter<TeamContract>(contract) {
    fun fragmentReady(idTeam: String) {
        getData(idTeam)
    }

    private fun getData(idTeam: String) {
        getView()?.loading(true, null)
        teamUseCase.getTeam(idTeam, object : BaseCallback<TeamModel>() {
            override fun onError(codeError: Int?, stringError: String?, exceptionError: Throwable?) {
                super.onError(codeError, stringError, exceptionError)
            }

            override fun onResponse(dataResponse: TeamModel) {
                super.onResponse(dataResponse)

                dataResponse.name?.let { getView()?.configureTitle(it) }
                getView()?.configureTabLayout()
                getView()?.configureViewPager(dataResponse)
                getView()?.loading(false, null)
            }
        })
    }
}

interface TeamContract : BaseContract {
    fun configureTitle(title: String)
    fun configureTabLayout()
    fun configureViewPager(dataResponse: TeamModel)
}

class TeamPagerAdapter(fm: FragmentManager, private val teamModel: TeamModel) : FragmentStatePagerAdapter(fm) {
    override fun getItem(position: Int): Fragment? {
        var fragment: Fragment? = null
        val args = Bundle()

        when (position) {
            0 -> {
                val teamStandingsFragment = TeamStandingsFragment()
                args.putSerializable("teamModel", teamModel)
                teamStandingsFragment.arguments = args
                fragment = teamStandingsFragment

            }
            1 -> {
                val teamRosterFragment = TeamRosterFragment()
                args.putSerializable("teamModel", teamModel)
                teamRosterFragment.arguments = args
                fragment = teamRosterFragment
            }
        }

        return fragment
    }

    override fun getCount(): Int {
        return NUM_PAGES
    }

}