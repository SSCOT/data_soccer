package com.sscot.soccerdataapp.features.team.data


import com.google.gson.annotations.SerializedName

data class TeamDTO(
    @SerializedName("generated_at")
    val generatedAt: String?,
    @SerializedName("jerseys")
    val jerseys: List<Jersey?>?,
    @SerializedName("manager")
    val manager: Manager?,
    @SerializedName("players")
    val players: List<Player>?,
    @SerializedName("schema")
    val schema: String?,
    @SerializedName("statistics")
    val statistics: Statistics?,
    @SerializedName("team")
    val team: Team?,
    @SerializedName("venue")
    val venue: Venue?
) {
    data class Jersey(
        @SerializedName("base")
        val base: String?,
        @SerializedName("horizontal_stripes")
        val horizontalStripes: Boolean?,
        @SerializedName("number")
        val number: String?,
        @SerializedName("shirt_type")
        val shirtType: String?,
        @SerializedName("sleeve")
        val sleeve: String?,
        @SerializedName("sleeve_detail")
        val sleeveDetail: String?,
        @SerializedName("split")
        val split: Boolean?,
        @SerializedName("squares")
        val squares: Boolean?,
        @SerializedName("stripes")
        val stripes: Boolean?,
        @SerializedName("type")
        val type: String?
    )

    data class Manager(
        @SerializedName("country_code")
        val countryCode: String?,
        @SerializedName("id")
        val id: String?,
        @SerializedName("name")
        val name: String?,
        @SerializedName("nationality")
        val nationality: String?
    )

    data class Player(
        @SerializedName("country_code")
        val countryCode: String?,
        @SerializedName("date_of_birth")
        val dateOfBirth: String?,
        @SerializedName("gender")
        val gender: String?,
        @SerializedName("height")
        val height: Int?,
        @SerializedName("id")
        val id: String?,
        @SerializedName("jersey_number")
        val jerseyNumber: Int?,
        @SerializedName("name")
        val name: String?,
        @SerializedName("nationality")
        val nationality: String?,
        @SerializedName("preferred_foot")
        val preferredFoot: String?,
        @SerializedName("type")
        val type: String?,
        @SerializedName("weight")
        val weight: Int?
    )

    data class Statistics(
        @SerializedName("seasons")
        val seasons: List<Season?>?
    ) {
        data class Season(
            @SerializedName("form")
            val form: Form?,
            @SerializedName("id")
            val id: String?,
            @SerializedName("name")
            val name: String?,
            @SerializedName("statistics")
            val statistics: Statistics?,
            @SerializedName("tournament")
            val tournament: Tournament?
        ) {
            data class Form(
                @SerializedName("away")
                val away: String?,
                @SerializedName("home")
                val home: String?,
                @SerializedName("total")
                val total: String?
            )

            data class Statistics(
                @SerializedName("goals_conceded")
                val goalsConceded: Int?,
                @SerializedName("goals_scored")
                val goalsScored: Int?,
                @SerializedName("group_position")
                val groupPosition: Int?,
                @SerializedName("matches_drawn")
                val matchesDrawn: Int?,
                @SerializedName("matches_lost")
                val matchesLost: Int?,
                @SerializedName("matches_played")
                val matchesPlayed: Int?,
                @SerializedName("matches_won")
                val matchesWon: Int?
            )

            data class Tournament(
                @SerializedName("category")
                val category: Category?,
                @SerializedName("id")
                val id: String?,
                @SerializedName("name")
                val name: String?,
                @SerializedName("sport")
                val sport: Sport?
            ) {
                data class Category(
                    @SerializedName("country_code")
                    val countryCode: String?,
                    @SerializedName("id")
                    val id: String?,
                    @SerializedName("name")
                    val name: String?
                )

                data class Sport(
                    @SerializedName("id")
                    val id: String?,
                    @SerializedName("name")
                    val name: String?
                )
            }
        }
    }

    data class Team(
        @SerializedName("abbreviation")
        val abbreviation: String?,
        @SerializedName("category")
        val category: Category?,
        @SerializedName("country")
        val country: String?,
        @SerializedName("country_code")
        val countryCode: String?,
        @SerializedName("id")
        val id: String?,
        @SerializedName("name")
        val name: String?,
        @SerializedName("sport")
        val sport: Sport?
    ) {
        data class Category(
            @SerializedName("country_code")
            val countryCode: String?,
            @SerializedName("id")
            val id: String?,
            @SerializedName("name")
            val name: String?
        )

        data class Sport(
            @SerializedName("id")
            val id: String?,
            @SerializedName("name")
            val name: String?
        )
    }

    data class Venue(
        @SerializedName("capacity")
        val capacity: Int?,
        @SerializedName("city_name")
        val cityName: String?,
        @SerializedName("country_code")
        val countryCode: String?,
        @SerializedName("country_name")
        val countryName: String?,
        @SerializedName("id")
        val id: String?,
        @SerializedName("map_coordinates")
        val mapCoordinates: String?,
        @SerializedName("name")
        val name: String?
    )
}