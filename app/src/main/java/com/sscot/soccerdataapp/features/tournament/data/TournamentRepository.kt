package com.sscot.soccerdataapp.features.tournament.data

import com.sscot.soccerdataapp.BaseCallback
import com.sscot.soccerdataapp.Result
import com.sscot.soccerdataapp.enqueue

class TournamentRepository(private val tournamentService: TournamentService) {

    fun getStandings(idTournament: String, callback: BaseCallback<List<TournamentDTO.Standing.Group.TeamStanding>>) {
        tournamentService.getStandings(idTournament).enqueue { result ->
            when (result) {
                is Result.Success -> {
                    result.response.body()?.let {
                        callback.onResponse(it.standings[0].groups[0].teamStandings)
                    }
                }

                is Result.Failure -> {
                    callback.onError(0, "No Data", null)
                }
            }
        }
    }

    fun getLeaders(idTournament: String, callback: BaseCallback<LeadersDTO>) {
        tournamentService.getLeaders(idTournament).enqueue { result ->
            when (result) {
                is Result.Success -> {
                    result.response.body()?.let {
                        callback.onResponse(it)
                    }
                }

                is Result.Failure -> {
                    callback.onError(0, "No Data", null)
                }
            }
        }
    }
}

