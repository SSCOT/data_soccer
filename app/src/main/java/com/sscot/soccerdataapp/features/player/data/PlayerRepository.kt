package com.sscot.soccerdataapp.features.tournament.data

import com.sscot.soccerdataapp.BaseCallback
import com.sscot.soccerdataapp.Result
import com.sscot.soccerdataapp.enqueue
import com.sscot.soccerdataapp.features.player.data.PlayerDTO

class PlayerRepository(private val playerService: PlayerService) {
    fun getPlayer(idPlayer: String, callback: BaseCallback<PlayerDTO>) {
        playerService.getPlayer(idPlayer).enqueue { result ->
            when (result) {
                is Result.Success -> {
                    result.response.body()?.let {
                        callback.onResponse(it)
                    }
                }

                is Result.Failure -> {
                    callback.onError(0, "No Data", null)
                }
            }
        }
    }
}

