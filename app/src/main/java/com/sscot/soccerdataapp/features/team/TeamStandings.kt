import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.sscot.soccerdataapp.BaseContract
import com.sscot.soccerdataapp.BasePresenter
import com.sscot.soccerdataapp.R
import com.sscot.soccerdataapp.features.team.data.TeamModel
import im.dacer.androidcharts.PieHelper
import kotlinx.android.synthetic.main.fragment_team_detail.*
import kotlinx.android.synthetic.main.fragment_team_detail_data.*
import kotlinx.android.synthetic.main.fragment_team_detail_standings.*
import kotlinx.android.synthetic.main.fragment_team_detail_standings_goals.*
import kotlinx.android.synthetic.main.fragment_team_detail_standings_matches.*
import org.koin.android.ext.android.inject
import org.koin.core.parameter.parametersOf

class TeamStandingsFragment : Fragment(), TeamStandingsContract {

    private val presenter: TeamStandingsPresenter by inject { parametersOf(this) }
    private lateinit var teamModel: TeamModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_team_detail, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val arguments = this.arguments
        arguments?.let {
            teamModel = it.getSerializable("teamModel") as TeamModel
        }

        presenter.fragmentViewCreated()
    }


    override fun showData() {

        teamModel.let {
            frg_team_detail_team_name.text = it.name

            showStadiumData(it)
            showManager(it)

            frg_team_detail_standings_title.setText(
                getString(
                    R.string.standings_on,
                    it.standingsLeague?.name
                )
            )

            frg_team_detail_standings_pos_txt.text = it.standingsLeague?.pos.toString()

            it.standingsLeague?.let { standings ->
                showDataLastMatches(standings)
                showDataMatches(it)
                showDataGoals(it)
            }
        }

        frg_team_detail_team.visibility = View.VISIBLE
    }

    private fun showStadiumData(teamModel: TeamModel) {
        teamModel.stadium?.let {
            frg_team_detail_txt_stadium.text = it.name
            frg_team_detail_txt_stadium_capacity.text =
                this.getString(R.string.stadium_capacity, it.capacity)
        }
    }

    private fun showManager(teamModel: TeamModel) {
        frg_team_detail_txt_manager.text = teamModel.manager?.name
        teamModel.manager?.country_flag_url?.let {
            Glide
                .with(this)
                .load(Uri.parse(it))
                .centerCrop()
                .placeholder(R.drawable.logo)
                .into(frg_team_detail_img_country_coach)
        }
    }

    private fun showDataLastMatches(standingsLeague: TeamModel.StandingsLeague) {

        frg_team_detail_standings_line_last_matches.apply {
            setDrawDotLine(false)
            setBottomTextList(standingsLeague.lastMatchesStrings)
            setColorArray(intArrayOf(ContextCompat.getColor(context, R.color.colorPrimary)))
            setDataList(arrayListOf(standingsLeague.lastMatches))
        }

    }

    private fun showDataMatches(team: TeamModel) {
        frg_team_detail_standings_pie_matches.apply {
            minimumHeight = 100
            minimumWidth = 100

            val pieMatchesData = ArrayList<PieHelper>()

            context?.let { context ->

                var totalPercent: Float = 0F
                team.standingsLeague?.let {
                    it.wonPercent?.let {
                        totalPercent += it
                        pieMatchesData.add(
                            PieHelper(
                                it,
                                ContextCompat.getColor(context, R.color.matchesWon)
                            )
                        )
                    }

                    it.lostPercent?.let {
                        totalPercent += it
                        pieMatchesData.add(
                            PieHelper(
                                it,
                                ContextCompat.getColor(context, R.color.matchesLoss)
                            )
                        )
                    }

                    it.drawPercent?.let {
                        pieMatchesData.add(
                            PieHelper(
                                100 - totalPercent,
                                ContextCompat.getColor(context, R.color.matchesDraw)
                            )
                        )
                    }

                    frg_team_detail_txt_matches_win.text = it.won.toString()
                    frg_team_detail_txt_matches_lose.text = it.lost.toString()
                    frg_team_detail_txt_matches_draw.text = it.draw.toString()
                }
            }

            setDate(pieMatchesData)
        }
    }

    private fun showDataGoals(team: TeamModel) {
        frg_team_detail_standings_bar_goals.context?.let { context ->
            team.standingsLeague?.let {
                frg_team_detail_standings_bar_goals.fgPaint.color = ContextCompat.getColor(context, R.color.colorPrimary)
                if (it.gf != null && it.ga != null) {

                    frg_team_detail_standings_bar_goals.setDataList(
                        arrayListOf(it.gf!!, it.ga!!),
                        if (it.gf!! >= it.ga!!) it.gf!! else it.ga!!
                    )

                    frg_team_detail_standings_bar_goals_for_txt.text = it.gf.toString()
                    frg_team_detail_standings_bar_goals_against_txt.text = it.ga.toString()
                    frg_team_detail_standings_bar_goals_dif_txt.text = it.gd.toString()
                }
            }
        }
    }

    override fun loading(loading: Boolean, info: String?) {

    }
}

class TeamStandingsPresenter(contract: TeamStandingsContract) : BasePresenter<TeamStandingsContract>(contract) {
    fun fragmentViewCreated() {
        getView()?.showData()
    }
}

interface TeamStandingsContract : BaseContract {
    fun showData()
}