package com.sscot.soccerdataapp.features.player.data

import java.io.Serializable

class PlayerModel(
    var id: String? = "",
    var name: String? = "",
    var lastName: String? = "",
    var height: String? = "",
    var weight: String? = "",
    var dateOfBirth: String? = "",
    var age: String? = "",
    var countryCode: String? = "",
    var nationality: String? = "",
    var team: String? = "",
    var type: String? = "",
    var preferredFoot: String? = "",
    var staticsHistory: ArrayList<TeamStatics>? = null,
    var staticsTotal: TeamStatics? = null
) : Serializable {

    data class TeamStatics(
        var seasonName: String? = "",
        var teamName: String? = "",
        var assists: String? = "",
        var goalsScored: String? = "",
        var matchesPlayed: String? = "",
        var ownGoals: String? = "",
        var redCards: String? = "",
        var yellowCards: String? = "",
        var yellowRedCards: String? = ""
    ) : Serializable

    fun getCompleteName(): String {
        name?.let { lastName?.let {
            return "$name $lastName"
        } }

        return ""
    }
}