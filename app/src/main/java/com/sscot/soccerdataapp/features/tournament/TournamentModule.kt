package com.sscot.soccerdataapp.features.tournament

import org.koin.dsl.module

val tournamentModule = module {
    factory { (tournamentContract: TournamentContract) -> TournamentPresenter(get(),tournamentContract) }
}

val standingsTournamentModule = module {
    factory { (standingsTournamentContract: StandingsTournamentContract) -> StandingsTournamentPresenter(standingsTournamentContract) }
}

val leadersTournamentModule = module {
    factory { (leadersTournamentContract: LeadersTournamentContract) -> LeadersTournamentPresenter(leadersTournamentContract) }
}