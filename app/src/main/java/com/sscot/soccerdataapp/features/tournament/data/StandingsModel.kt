package com.sscot.soccerdataapp.features.tournament.data

import java.io.Serializable

class StandingsModel(
    var id: String = "",
    var rank: String = "",
    var name: String = "",
    var current_outcome: String = "",
    var points: String = "",
    var matches_played: String = "",
    var matches_win: String = "",
    var matches_draw: String = "",
    var matches_lose: String = "",
    var goals_for: String = "",
    var goals_against: String = ""
) : Serializable