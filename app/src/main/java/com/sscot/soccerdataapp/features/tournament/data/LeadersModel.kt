package com.sscot.soccerdataapp.features.tournament.data

import java.io.Serializable

class LeadersModel(
    var topGoals: ArrayList<TopGoal> = ArrayList(),
    var topAssists: ArrayList<TopAssist> = ArrayList()
) : Serializable {
    data class TopGoal(
        var name: String? = "",
        var rank: String = "",
        var team: String? = "",
        var points: String = ""
    ) : Serializable

    data class TopAssist(
        var name: String? = "",
        var rank: String = "",
        var team: String? = "",
        var points: String = ""
    ) : Serializable
}