package com.sscot.soccerdataapp.features.team.data

import android.graphics.Color
import com.sscot.soccerdataapp.*
import com.sscot.soccerdataapp.features.tournament.data.TeamRepository
import java.util.*

class TeamUseCase(private val teamRepository: TeamRepository) : BaseClassExtensions() {
    fun getTeam(idTeam: String, callback: BaseCallback<TeamModel>) {
        teamRepository.getTeam(idTeam, object : BaseCallback<TeamDTO>() {
            override fun onError(codeError: Int?, stringError: String?, exceptionError: Throwable?) {
                super.onError(codeError, stringError, exceptionError)
            }

            override fun onResponse(dataResponse: TeamDTO) {
                super.onResponse(dataResponse)

                callback.onResponse(proccessData(dataResponse))
            }
        })
    }

    fun proccessData(data: TeamDTO): TeamModel {
        val teamModel: TeamModel = TeamModel()

        teamModel.name = data.team?.name

        data.venue?.let {
            teamModel.stadium = TeamModel.Stadium(
                name = it.name,
                capacity = it.capacity.toString().formatNumberWithDots(),
                city = it.cityName
            )
        }

        data.manager?.let {
            teamModel.manager = TeamModel.Manager(
                name = it.name?.let {
                    val nameWithFormat = it.split(",")
                    if (nameWithFormat.size > 1)
                        nameWithFormat[1] + " " + nameWithFormat[0]
                    else
                        nameWithFormat[0]
                } ?: run {
                    ""
                },
                country_flag_url = it.countryCode?.let {
                    val format =
                        String.format(BuildConfig.FLAGS_URL_BASE, it.toLowerCase().substring(0, 2))
                    format
                } ?: run {
                    null
                }
            )
        }

        data.statistics?.seasons?.let {
            val season = it[it.size - 1]
            season?.let {

                val standingsLeague = TeamModel.StandingsLeague()

                standingsLeague.name = it.name
                it.statistics?.let {
                    standingsLeague.pos = it.groupPosition
                    standingsLeague.won = it.matchesWon
                    standingsLeague.draw = it.matchesDrawn
                    standingsLeague.lost = it.matchesLost

                    if (it.matchesWon != null && it.matchesPlayed != null)
                        standingsLeague.wonPercent = it.matchesPlayed.calculatePercent(it.matchesWon)
                    if (it.matchesDrawn != null && it.matchesPlayed != null)
                        standingsLeague.drawPercent = it.matchesPlayed.calculatePercent(it.matchesDrawn)
                    if (it.matchesLost != null && it.matchesPlayed != null)
                        standingsLeague.lostPercent = it.matchesPlayed.calculatePercent(it.matchesLost)

                    standingsLeague.gf = it.goalsScored
                    standingsLeague.ga = it.goalsConceded
                    if (it.goalsConceded != null && it.goalsScored != null)
                        standingsLeague.gd = it.goalsScored - it.goalsConceded
                }

                it.form?.total?.let {

                    it.forEach {
                        when (it) {
                            'W' -> {
                                standingsLeague.apply {
                                    lastMatches.add(2)
                                    lastMatchesStrings.add(it.toString())
                                }
                            }
                            'D' -> {
                                standingsLeague.apply {
                                    lastMatches.add(0)
                                    lastMatchesStrings.add(it.toString())
                                }
                            }
                            'L' -> {
                                standingsLeague.apply {
                                    lastMatches.add(-2)
                                    lastMatchesStrings.add(it.toString())
                                }
                            }
                        }
                    }
                }

                teamModel.standingsLeague = standingsLeague
            }
        }

        data.players?.let {

            teamModel.players = ArrayList<TeamModel.Player>()

            it.forEach {
                var newPlayer = TeamModel.Player()
                newPlayer.apply {
                    id = it.id
                    name = it.name
                    age = it.dateOfBirth?.let { it.calculateAge() } ?: run { " - " }
                    country_code = it.countryCode?.substring(0, 2)
                    height = it.height?.let { it.toString() + " " + App.getContext()?.resources?.getString(R.string.height_unity) } ?: run { " - " }
                    weight = it.weight?.let { it.toString() + " " + App.getContext()?.resources?.getString(R.string.weight_unity) } ?: run { " - " }
                    it.jerseyNumber?.let { jersey_number = it.toString() }
                    preferred_foot = when (it.preferredFoot) {
                        "left" -> App.getContext()?.resources?.getString(R.string.left_foot)
                        "right" -> App.getContext()?.resources?.getString(R.string.right_foot)
                        else -> App.getContext()?.resources?.getString(R.string.right_foot)
                    }

                    when (it.type) {
                        "goalkeeper" -> position = "POR"
                        "defender" -> position = "DEF"
                        "midfielder" -> position = "MED"
                        "forward" -> position = "DEL"
                    }

                    preferred_foot = it.preferredFoot
                }
                teamModel.players!!.add(newPlayer)
            }
        }

        data.jerseys?.get(0)?.let {
            teamModel.teamColor = Color.parseColor("#" + it.base)
        }

        return teamModel
    }
}