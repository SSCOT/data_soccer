package com.sscot.soccerdataapp.features.tournament.data

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

interface TournamentService {
    @GET("soccer-t3/eu/en/tournaments/{idTournament}/standings.json?api_key=nvnugr54fmcvtupxswfcv7yq")
    fun getStandings(@Path("idTournament") idTournament: String): Call<TournamentDTO>

    @GET("soccer-t3/eu/en/tournaments/{idTournament}/leaders.json?api_key=nvnugr54fmcvtupxswfcv7yq")
    fun getLeaders(@Path("idTournament") idTournament: String) : Call<LeadersDTO>
}