package com.sscot.soccerdataapp.features.tournament.data

import com.sscot.soccerdataapp.BaseCallback

class TournamentUseCase(private val tournamentRepository: TournamentRepository) {

    fun getStandings(idTournament: String, callback: BaseCallback<List<StandingsModel>>) {
        tournamentRepository.getStandings(idTournament, object : BaseCallback<List<TournamentDTO.Standing.Group.TeamStanding>>() {
                override fun onError(
                    codeError: Int?,
                    stringError: String?,
                    exceptionError: Throwable?
                ) {
                    super.onError(codeError, stringError, exceptionError)
                }

                override fun onResponse(standingsDataResponse: List<TournamentDTO.Standing.Group.TeamStanding>) {
                    super.onResponse(standingsDataResponse)
                    callback.onResponse(proccessDataStandings(standingsDataResponse))
                }
            })
    }

    fun getLeaders(idTournament: String, callback: BaseCallback<LeadersModel>) {
        tournamentRepository.getLeaders(idTournament, object : BaseCallback<LeadersDTO>() {
            override fun onError(
                codeError: Int?,
                stringError: String?,
                exceptionError: Throwable?
            ) {
                super.onError(codeError, stringError, exceptionError)
            }

            override fun onResponse(leadersDataResponse: LeadersDTO) {
                super.onResponse(leadersDataResponse)

                callback.onResponse(proccessLeadersData(leadersDataResponse))
            }
        })
    }

    fun proccessDataStandings(data: List<TournamentDTO.Standing.Group.TeamStanding>): List<StandingsModel> {
        val listStandingsModel: ArrayList<StandingsModel> = ArrayList()

        data.forEach { data ->
            val standingsModel: StandingsModel = StandingsModel()
            standingsModel.apply {
                id = data.team.id
                rank = data.rank.toString()
                name = data.team.name.toString()
                current_outcome = data.currentOutcome.toString()
                points = data.points.toString()
                matches_played = data.played.toString()
                matches_win = data.win.toString()
                matches_draw = data.draw.toString()
                matches_lose = data.loss.toString()
                goals_against = data.goalsAgainst.toString()
                goals_for = data.goalsFor.toString()
            }

            listStandingsModel.add(standingsModel)
        }

        return listStandingsModel
    }

    fun proccessLeadersData(data: LeadersDTO): LeadersModel {

        val leadersModel: LeadersModel = LeadersModel()

        val topGoals = data.topGoals?.subList(0, 5)
        val topAssists = data.topAssists?.subList(0, 5)

        topGoals?.forEach {
            leadersModel.topGoals.add(
                LeadersModel.TopGoal(
                    it.player?.name.toString(),
                    it.rank.toString(),
                    it.team?.abbreviation.toString(),
                    it.goals.toString()
                )
            )
        }

        topAssists?.forEach {
            leadersModel.topAssists.add(
                LeadersModel.TopAssist(
                    it.player?.name.toString(),
                    it.rank.toString(),
                    it.team?.abbreviation.toString(),
                    it.assists.toString()
                )
            )
        }

        return leadersModel
    }
}