package com.sscot.soccerdataapp.features.tournament.data


import com.google.gson.annotations.SerializedName

data class TournamentDTO(
    @SerializedName("generated_at")
    val generatedAt: String,
    @SerializedName("schema")
    val schema: String,
    @SerializedName("season")
    val season: Season,
    @SerializedName("standings")
    val standings: List<Standing>,
    @SerializedName("tournament")
    val tournament: Tournament
) {
    data class Season(
        @SerializedName("end_date")
        val endDate: String,
        @SerializedName("id")
        val id: String,
        @SerializedName("name")
        val name: String,
        @SerializedName("start_date")
        val startDate: String,
        @SerializedName("tournament_id")
        val tournamentId: String,
        @SerializedName("year")
        val year: String
    )

    data class Standing(
        @SerializedName("groups")
        val groups: List<Group>,
        @SerializedName("tie_break_rule")
        val tieBreakRule: String,
        @SerializedName("type")
        val type: String
    ) {
        data class Group(
            @SerializedName("id")
            val id: String,
            @SerializedName("team_standings")
            val teamStandings: List<TeamStanding>
        ) {
            data class TeamStanding(
                @SerializedName("change")
                val change: Int,
                @SerializedName("current_outcome")
                val currentOutcome: String?,
                @SerializedName("draw")
                val draw: Int,
                @SerializedName("goal_diff")
                val goalDiff: Int,
                @SerializedName("goals_against")
                val goalsAgainst: Int,
                @SerializedName("goals_for")
                val goalsFor: Int,
                @SerializedName("loss")
                val loss: Int,
                @SerializedName("played")
                val played: Int,
                @SerializedName("points")
                val points: Int,
                @SerializedName("rank")
                val rank: Int,
                @SerializedName("team")
                val team: Team,
                @SerializedName("win")
                val win: Int
            ) {
                data class Team(
                    @SerializedName("id")
                    val id: String,
                    @SerializedName("name")
                    val name: String
                )
            }
        }
    }

    data class Tournament(
        @SerializedName("category")
        val category: Category,
        @SerializedName("current_season")
        val currentSeason: CurrentSeason,
        @SerializedName("id")
        val id: String,
        @SerializedName("name")
        val name: String,
        @SerializedName("sport")
        val sport: Sport
    ) {
        data class Category(
            @SerializedName("country_code")
            val countryCode: String,
            @SerializedName("id")
            val id: String,
            @SerializedName("name")
            val name: String
        )

        data class CurrentSeason(
            @SerializedName("end_date")
            val endDate: String,
            @SerializedName("id")
            val id: String,
            @SerializedName("name")
            val name: String,
            @SerializedName("start_date")
            val startDate: String,
            @SerializedName("year")
            val year: String
        )

        data class Sport(
            @SerializedName("id")
            val id: String,
            @SerializedName("name")
            val name: String
        )
    }
}