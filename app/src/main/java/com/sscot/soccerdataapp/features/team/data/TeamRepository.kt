package com.sscot.soccerdataapp.features.tournament.data

import com.sscot.soccerdataapp.BaseCallback
import com.sscot.soccerdataapp.Result
import com.sscot.soccerdataapp.enqueue
import com.sscot.soccerdataapp.features.team.data.TeamDTO

class TeamRepository(private val teamService: TeamService) {
    fun getTeam(idTeam: String, callback: BaseCallback<TeamDTO>) {
        teamService.getTeam(idTeam).enqueue { result ->
            when (result) {
                is Result.Success -> {
                    result.response.body()?.let {
                        callback.onResponse(it)
                    }
                }

                is Result.Failure -> {
                    callback.onError(0, "No Data", null)
                }
            }
        }
    }
}

