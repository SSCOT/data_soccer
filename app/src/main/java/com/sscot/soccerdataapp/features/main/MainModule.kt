package com.sscot.soccerdataapp.features.main

import org.koin.dsl.module

val mainModule = module {
    factory { (mainContract:MainContract) -> MainPresenter(mainContract) }
}