package com.sscot.soccerdataapp.features.player.data

import com.sscot.soccerdataapp.BaseCallback
import com.sscot.soccerdataapp.BaseClassExtensions
import com.sscot.soccerdataapp.features.tournament.data.PlayerRepository

class PlayerUseCase(private val playerRepository: PlayerRepository) : BaseClassExtensions() {
    fun getPlayer(idPlayer: String, callback: BaseCallback<PlayerModel>) {
        playerRepository.getPlayer(idPlayer, object : BaseCallback<PlayerDTO>() {
            override fun onError(codeError: Int?, stringError: String?, exceptionError: Throwable?) {
                super.onError(codeError, stringError, exceptionError)
            }

            override fun onResponse(dataResponse: PlayerDTO) {
                super.onResponse(dataResponse)

                callback.onResponse(proccessData(dataResponse))
            }
        })
    }

    fun proccessData(data: PlayerDTO): PlayerModel {
        val playerModel: PlayerModel = PlayerModel()

        playerModel.apply {
            data.player?.let {
                id = it.id
                it.name?.split(",")?.let {
                        na -> name = na[na.size - 1]
                }
                lastName = it.lastName
                dateOfBirth = it.dateOfBirth
                age = dateOfBirth?.calculateAge()
                height = it.height.toString()
                weight = it.weight.toString()
                countryCode = it.countryCode
                nationality = it.nationality
                type = it.type
                preferredFoot = it.preferredFoot
            }

            data.statistics?.let {
                it.totals?.let {
                    staticsTotal = PlayerModel.TeamStatics(
                        "",
                        "",
                        it.assists.toString(),
                        it.goalsScored.toString(),
                        it.matchesPlayed.toString(),
                        it.ownGoals.toString(),
                        it.redCards.toString(),
                        it.yellowCards.toString(),
                        it.yellowRedCards.toString()
                    )
                }

                it.seasons?.let {

                    staticsHistory = ArrayList<PlayerModel.TeamStatics>()

                    it.forEach {
                        val seasonStatic = PlayerModel.TeamStatics()

                        it?.let {
                            seasonStatic.apply {
                                seasonName = it.name
                                teamName = it.team?.name
                                it.statistics?.let {
                                    assists = it.assists.toString()
                                    goalsScored = it.goalsScored.toString()
                                    matchesPlayed = it.matchesPlayed.toString()
                                    ownGoals = it.ownGoals.toString()
                                    redCards = it.redCards.toString()
                                    yellowCards = it.yellowCards.toString()
                                    yellowRedCards = it.yellowRedCards.toString()
                                }
                            }

                            staticsHistory?.add(seasonStatic)
                        }
                    }
                }
            }
        }

        return playerModel
    }
}
