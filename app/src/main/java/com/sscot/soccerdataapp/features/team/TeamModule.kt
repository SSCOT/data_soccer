package com.sscot.soccerdataapp.features.team

import TeamStandingsContract
import TeamStandingsPresenter
import org.koin.dsl.module

val teamModule = module {
    factory { (teamContract: TeamContract) -> TeamPresenter(get(),teamContract) }
}

val teamRosterModule = module {
    factory { (teamRosterContract: TeamRosterContract) -> TeamRosterPresenter(teamRosterContract) }
}

val teamStandingsModule = module {
    factory { (teamStandingsContract: TeamStandingsContract) -> TeamStandingsPresenter(teamStandingsContract) }
}