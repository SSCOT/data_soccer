package com.sscot.soccerdataapp.features.tournament.data

import com.sscot.soccerdataapp.features.team.data.TeamDTO
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

interface TeamService {
    @GET("soccer-t3/eu/es/teams/{idTeam}/profile.json?api_key=nvnugr54fmcvtupxswfcv7yq")
    fun getTeam(@Path("idTeam") idTeam: String): Call<TeamDTO>
}