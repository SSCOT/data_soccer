package com.sscot.soccerdataapp.features.main

import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.FragmentTransaction
import com.google.android.material.navigation.NavigationView
import com.sscot.soccerdataapp.BaseContract
import com.sscot.soccerdataapp.BasePresenter
import com.sscot.soccerdataapp.BuildConfig
import com.sscot.soccerdataapp.R
import com.sscot.soccerdataapp.features.tournament.TournamentFragment
import kotlinx.android.synthetic.main.activity_main.*
import org.koin.android.ext.android.inject
import org.koin.core.parameter.parametersOf

class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener,
    MainContract {

    private val presenter: MainPresenter by inject { parametersOf(this) }
    private lateinit var sidebarToggle: ActionBarDrawerToggle


    override fun loading(loading: Boolean, info: String?) {
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.AppTheme_NoActionBar)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setSupportActionBar(toolbar)
        val sidebar: DrawerLayout = main_drawer
        sidebarToggle = ActionBarDrawerToggle(this, sidebar, R.string.sideBarOpen, R.string.sideBarClose)
        sidebar.addDrawerListener(sidebarToggle)
        sidebarToggle.syncState()
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        presenter.onCreate()
    }

    override fun setupNavigationDrawerContent() {
        mainNavigationView.setNavigationItemSelectedListener(this)
    }

    override fun onNavigationItemSelected(menuItem: MenuItem): Boolean {
        presenter.selectSection(menuItem.itemId)
        return true
    }

    override fun showTournament(idTournament: String, nameTournament: String) {

        main_drawer.closeDrawers()

        val transaction: FragmentTransaction = supportFragmentManager.beginTransaction()

        val tournamentFragment = TournamentFragment()

        val bundle: Bundle = Bundle()
        bundle.putString("idTournament", idTournament)
        bundle.putString("nameTournament", nameTournament)
        tournamentFragment.arguments = bundle
        transaction.replace(R.id.container, tournamentFragment)
        transaction.commit()
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (sidebarToggle.onOptionsItemSelected(item))
            return true

        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {

    }
}

class MainPresenter(contract: MainContract) : BasePresenter<MainContract>(contract) {

    fun onCreate() {
        getView()?.setupNavigationDrawerContent()
    }

    fun selectSection(section: Int) {
        when (section) {
            R.id.drawerNavLaLiga -> { getView()?.showTournament(BuildConfig.ID_LALIGA,BuildConfig.NAME_LALIGA) }
            R.id.drawerNavPremiereLeague -> { getView()?.showTournament(BuildConfig.ID_PREMIER, BuildConfig.NAME_PREMIER) }
            R.id.drawerNavSerieA -> getView()?.showTournament(BuildConfig.ID_SERIEA, BuildConfig.NAME_SERIEA)
            R.id.drawerNavBundesliga -> getView()?.showTournament(BuildConfig.ID_BUNDESLIGA, BuildConfig.NAME_BUNDESLIGA)
            R.id.drawerNavLigue1 -> getView()?.showTournament(BuildConfig.ID_LIGUE1, BuildConfig.NAME_LIGUE1)
        }
    }
}

interface MainContract : BaseContract {
    fun setupNavigationDrawerContent()
    fun showTournament(id: String, name: String)
}
