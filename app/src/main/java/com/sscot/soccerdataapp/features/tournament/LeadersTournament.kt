package com.sscot.soccerdataapp.features.tournament

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.sscot.soccerdataapp.BaseContract
import com.sscot.soccerdataapp.BasePresenter
import com.sscot.soccerdataapp.R
import com.sscot.soccerdataapp.features.tournament.data.LeadersModel
import kotlinx.android.synthetic.main.fragment_leaders_tournament.*
import kotlinx.android.synthetic.main.view_holder_leaders_tournament.view.*
import org.koin.android.ext.android.inject
import org.koin.core.parameter.parametersOf

class LeadersTournamentFragment : Fragment(), LeadersTournamentContract {

    private val presenter: LeadersTournamentPresenter by inject { parametersOf(this) }
    private var leadersData: LeadersModel? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_leaders_tournament, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val arguments = this.arguments
        arguments?.let {
            leadersData = it.getSerializable("leadersModel") as LeadersModel
        }

        configRecyclers()
        presenter.fragmentViewCreated()
    }

    private fun configRecyclers() {
        leaders_tournament_goal_recycler_view.let {
            it.layoutManager = LinearLayoutManager(context)
            it.setHasFixedSize(true)
        }

        leaders_tournament_assist_recycler_view.let {
            it.layoutManager = LinearLayoutManager(context)
            it.setHasFixedSize(true)
        }
    }

    override fun showLeaders() {

        leadersData?.let {

            val mLayoutManagerGoals = LinearLayoutManager(context)
            it.topGoals.let {
                leaders_tournament_goal_recycler_view.apply {
                    setHasFixedSize(true)
                    layoutManager = mLayoutManagerGoals
                    adapter = TopGoalTournamentAdapter(it)
                }
            }

            val mLayoutManagerAssists = LinearLayoutManager(context)
            it.topAssists.let {
                leaders_tournament_assist_recycler_view.apply {
                    setHasFixedSize(true)
                    layoutManager = mLayoutManagerAssists
                    adapter = TopAssistTournamentAdapter(it)
                }
            }
        }
    }

    override fun loading(loading: Boolean, info: String?) {

    }

}

class LeadersTournamentPresenter(contract: LeadersTournamentContract) : BasePresenter<LeadersTournamentContract>(contract) {
    fun fragmentViewCreated() {
        getView()?.showLeaders()
    }
}

interface LeadersTournamentContract : BaseContract {
    fun showLeaders()
}

class TopGoalTournamentAdapter(val playerList: List<LeadersModel.TopGoal>) :
    RecyclerView.Adapter<TopGoalTournamentAdapter.PlayerGridViewHolder>() {

    private var context: Context? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PlayerGridViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.view_holder_leaders_tournament, parent, false)
        context = parent.context
        return PlayerGridViewHolder(view)
    }

    override fun getItemCount(): Int {
        return playerList.size
    }

    override fun onBindViewHolder(holder: PlayerGridViewHolder, position: Int) {
        holder.onBind(playerList.get(position), position == 0)
    }


    inner class PlayerGridViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun onBind(player: LeadersModel.TopGoal, leader: Boolean) {
            player.let {
                itemView.vh_leaders_tournaments_rank.text = it.rank

                val nameArray = it.name?.split(",")
                nameArray?.let {
                    if (it.size > 1)
                        itemView.vh_leaders_tournaments_name.text = it[1] + " " + it[0]
                    else
                        itemView.vh_leaders_tournaments_name.text = it[0]
                }

                itemView.vh_leaders_tournaments_points.text = it.points

                if (leader) {
                    itemView.vh_leaders_tournaments_name.textSize = 24F
                } else
                    itemView.vh_leaders_tournaments_name.textSize = 18F
            }
        }
    }

}

class TopAssistTournamentAdapter(val playerList: List<LeadersModel.TopAssist>) :
    RecyclerView.Adapter<TopAssistTournamentAdapter.PlayerGridViewHolder>() {

    private var context: Context? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PlayerGridViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.view_holder_leaders_tournament, parent, false)
        context = parent.context
        return PlayerGridViewHolder(view)
    }

    override fun getItemCount(): Int {
        return playerList.size
    }

    override fun onBindViewHolder(holder: PlayerGridViewHolder, position: Int) {
        holder.onBind(playerList.get(position), position == 0)
    }


    inner class PlayerGridViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun onBind(player: LeadersModel.TopAssist, leader: Boolean) {

            player.let {
                itemView.vh_leaders_tournaments_rank.text = it.rank

                val nameArray = it.name?.split(",")
                nameArray?.let {
                    if (it.size > 1)
                        itemView.vh_leaders_tournaments_name.text = it[1] + " " + it[0]
                    else
                        itemView.vh_leaders_tournaments_name.text = it[0]
                }

                itemView.vh_leaders_tournaments_points.text = it.points

                if (leader) {
                    itemView.vh_leaders_tournaments_name.textSize = 24F
                } else
                    itemView.vh_leaders_tournaments_name.textSize = 18F
            }

        }
    }
}
