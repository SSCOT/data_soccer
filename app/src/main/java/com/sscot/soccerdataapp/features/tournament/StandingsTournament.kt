package com.sscot.soccerdataapp.features.tournament

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.sscot.soccerdataapp.BaseContract
import com.sscot.soccerdataapp.BasePresenter
import com.sscot.soccerdataapp.R
import com.sscot.soccerdataapp.features.team.TeamActivity
import com.sscot.soccerdataapp.features.tournament.data.StandingsModel
import kotlinx.android.synthetic.main.fragment_standings_tournament.*
import kotlinx.android.synthetic.main.view_holder_standings_tournament_team.view.*
import org.koin.android.ext.android.inject
import org.koin.core.parameter.parametersOf

class StandingsTournamentFragment : Fragment(), StandingsTournamentContract {

    private val presenter: StandingsTournamentPresenter by inject { parametersOf(this) }
    private var standingsData: ArrayList<StandingsModel>? = null


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_standings_tournament, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val arguments = this.arguments
        arguments?.let {
            standingsData = it.getSerializable("standigsModel") as ArrayList<StandingsModel>?
        }

        presenter.fragmentViewCreated()
    }


    override fun showStandings() {

        standingsData?.let {
            val mLayoutManager = LinearLayoutManager(context)

            standings_tournament_recycler_view.apply {
                setHasFixedSize(true)
                layoutManager = mLayoutManager
                adapter = StandingsTournamentAdapter(it, { teamStanding -> onTeamClick(teamStanding) })
            }
        }
    }

    override fun launchTeamDetail(idTeam: String) {
        val intent = Intent(activity, TeamActivity::class.java).apply {
            putExtra("idTeam", idTeam)
        }
        startActivity(intent)
    }

    private fun onTeamClick(teamStanding: StandingsModel) {
        presenter.teamClicked(teamStanding)
    }

    override fun loading(loading: Boolean, info: String?) {

    }
}

class StandingsTournamentPresenter(contract: StandingsTournamentContract) : BasePresenter<StandingsTournamentContract>(contract) {

    fun fragmentViewCreated() {
        getView()?.showStandings()
    }

    fun teamClicked(teamStanding: StandingsModel) {
        getView()?.launchTeamDetail(teamStanding.id)
    }
}

interface StandingsTournamentContract : BaseContract {
    fun showStandings()
    fun launchTeamDetail(idTeam: String)
}

class StandingsTournamentAdapter(val teamsList: List<StandingsModel>, val onClickListener: (StandingsModel) -> Unit) : RecyclerView.Adapter<StandingsTournamentAdapter.TeamGridViewHolder>() {

    private var context: Context? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TeamGridViewHolder {

        context = parent.context
        return TeamGridViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.view_holder_standings_tournament_team,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return teamsList.size
    }

    override fun onBindViewHolder(holder: TeamGridViewHolder, position: Int) {
        holder.onBind(teamsList.get(position))
    }

    inner class TeamGridViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun onBind(teamStanding: StandingsModel) {
            teamStanding.let {
                itemView.apply {
                    vh_standings_tournaments_rank.text = it.rank
                    vh_standings_tournaments_name.text = it.name
                    it.current_outcome?.let {

                        var standingColor: Int? = null

                        when (it) {
                            "Champions League", "Champions League Qualification" -> standingColor = ContextCompat.getColor(context!!, R.color.colorChampionsLeague)
                            "Europa League", "Europa League Qualification" -> standingColor = ContextCompat.getColor(context!!, R.color.colorEuropaLeague)
                            "Relegation" -> standingColor = ContextCompat.getColor(context!!, R.color.colorRelegation)
                        }

                        standingColor?.let {
                            vh_standings_tournaments_name.setBackgroundColor(it)
                        }
                    }

                    vh_standings_tournaments_points.text = it.points
                    vh_standings_tournaments_played.text = it.matches_played
                    vh_standings_tournaments_win.text = it.matches_win
                    vh_standings_tournaments_draw.text = it.matches_draw
                    vh_standings_tournaments_loss.text = it.matches_lose
                    vh_standings_tournaments_goals_for.text = it.goals_for
                    vh_standings_tournaments_goals_against.text = it.goals_against
                    setOnClickListener { onClickListener(teamStanding) }
                }
            }
        }
    }

}