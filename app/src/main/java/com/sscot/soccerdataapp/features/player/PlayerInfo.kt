package com.sscot.soccerdataapp.features.player

import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.sscot.soccerdataapp.BaseContract
import com.sscot.soccerdataapp.BasePresenter
import com.sscot.soccerdataapp.BuildConfig
import com.sscot.soccerdataapp.R
import com.sscot.soccerdataapp.features.team.data.TeamModel
import kotlinx.android.synthetic.main.fragment_team_roster.*
import kotlinx.android.synthetic.main.view_holder_team_schedule.view.*
import org.koin.android.ext.android.inject
import org.koin.core.parameter.parametersOf

class PlayerInfoFragment : Fragment(), PlayerInfoContract {

    // TODO hay que implementar esto

    private val presenter: PlayerInfoPresenter by inject { parametersOf(this) }
    private lateinit var teamModel: TeamModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_team_roster, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val arguments = this.arguments
        arguments?.let {
            teamModel = it.getSerializable("teamModel") as TeamModel
        }

        presenter.fragmentViewCreated()
    }

    override fun showData() {

        teamModel.standingsLeague?.let {
            configurePlayersRecycler(teamModel)
        }

        // frg_team_detail_team.visibility = View.VISIBLE
    }

    private fun configurePlayersRecycler(teamModel: TeamModel) {
        /*val mLayoutManager = LinearLayoutManager(context)

        teamModel.players?.let {
            frg_team_detail_player_recycler.apply {
                setHasFixedSize(true)
                layoutManager = mLayoutManager
                adapter = PlayersTeamAdapter(it)
            }
        }*/
    }

    override fun loading(loading: Boolean, info: String?) {

    }
}

class PlayerInfoPresenter(contract: PlayerInfoContract) : BasePresenter<PlayerInfoContract>(contract) {
    fun fragmentViewCreated() {
        getView()?.showData()
    }

}

interface PlayerInfoContract : BaseContract {
    fun showData()
}


/*class PlayersTeamAdapter(val playerList: List<TeamModel.Player>) :
    RecyclerView.Adapter<PlayersTeamAdapter.PlayerViewHolder>() {

    private var context: Context? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PlayerViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.view_holder_team_schedule, parent, false)
        context = parent.context
        return PlayerViewHolder(view)
    }

    override fun getItemCount(): Int {
        return playerList.size
    }

    override fun onBindViewHolder(holder: PlayerViewHolder, position: Int) {
        holder.onBind(playerList.get(position))
    }


    inner class PlayerViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun onBind(player: TeamModel.Player) {

            context?.let { context ->

                Glide
                    .with(context)
                    .load(
                        Uri.parse(
                            String.format(
                                BuildConfig.FLAGS_URL_BASE,
                                player.country_code?.substring(0, 2)
                            )
                        )
                    )
                    .centerCrop()
                    .into(itemView.vh_players_team_flag)

                itemView.vh_players_team_position.text = player.position
                itemView.vh_players_team_jersey_number.text = player.jersey_number.toString()
                player.name?.split(",")?.let {
                    when (it.size) {
                        1 -> itemView.vh_players_team_name.text = it[0]
                        2 -> itemView.vh_players_team_name.text = it[1] + " " + it[0]
                        else -> itemView.vh_players_team_name.text = it[0]
                    }
                }
                itemView.vh_players_team_age.text = player.age
                itemView.vh_players_team_height.text = player.height
                itemView.vh_players_team_weight.text = player.weight

                when (player.preferred_foot) {
                    "left" -> itemView.vh_players_team_preferred_foot.text = context.getString(R.string.left_foot)
                    "right" -> itemView.vh_players_team_preferred_foot.text = context.getString(R.string.right_foot)
                    else -> itemView.vh_players_team_preferred_foot.text = " - "
                }
            }
        }
    }
}*/
