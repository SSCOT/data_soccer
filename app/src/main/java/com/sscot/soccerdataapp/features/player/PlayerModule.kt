package com.sscot.soccerdataapp.features.player

import PlayerStandingsContract
import PlayerStandingsPresenter
import TeamStandingsContract
import TeamStandingsPresenter
import org.koin.dsl.module

val playerModule = module {
    factory { (playerContract: PlayerContract) -> PlayerPresenter(get(), playerContract) }
}

val playerStandingsModule = module {
    factory { (playerStandingsContract: PlayerStandingsContract) -> PlayerStandingsPresenter(playerStandingsContract) }
}

val playerInfoModule = module {
    factory { (playerInfoContract: PlayerInfoContract) -> PlayerInfoPresenter(playerInfoContract) }
}
