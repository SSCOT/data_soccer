package com.sscot.soccerdataapp.features.team.data

import java.io.Serializable

class TeamModel (

    var name: String? = "",
    var stadium: Stadium? = null,
    var manager: Manager? = null,
    var standingsLeague: StandingsLeague? = null,
    var standingsInternational: StandingsInternational? = null,
    var players: ArrayList<Player>? = null,
    var teamColor: Int = 0
) : Serializable {
    data class Stadium(
      var name:String? = "",
      var capacity: String? = "",
      var city: String? = ""
    ) : Serializable

    data class Manager(
        var name: String? = "",
        var country_flag_url: String? = ""
    ) : Serializable

    data class StandingsLeague(
        var name: String? = "",
        var pos: Int? = 0,
        var lastMatches: ArrayList<Int> = arrayListOf(),
        var lastMatchesStrings: ArrayList<String> = arrayListOf(),
        var played: Int? = 0,
        var won: Int? = 0,
        var draw: Int? = 0,
        var lost: Int? = 0,
        var wonPercent: Float? = null,
        var drawPercent: Float? = null,
        var lostPercent: Float? = null,
        var gf: Int? = 0,
        var ga: Int? = 0,
        var gd: Int? = 0
    ) : Serializable

    data class StandingsInternational(
        var pos: Int? = 0,
        var lastMatches: String? = "",
        var played: Int? = 0,
        var won: Int? = 0,
        var draw: Int? = 0,
        var lost: Int? = 0,
        var gf: Int? = 0,
        var gc: Int? = 0,
        var gd: Int? = 0
    ) : Serializable

    data class Player(
        var id: String? = "",
        var country_code: String? = "",
        var jersey_number: String? = "",
        var position: String = "",
        var name: String? = "",
        var age: String? = "",
        var height: String? = "",
        var weight: String? = "",
        var preferred_foot: String? = ""
    ) : Serializable
}