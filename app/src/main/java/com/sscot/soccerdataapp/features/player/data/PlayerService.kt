package com.sscot.soccerdataapp.features.tournament.data

import com.sscot.soccerdataapp.features.player.data.PlayerDTO
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

interface PlayerService {
    @GET("soccer-t3/eu/en/players/{idPlayer}/profile.json?api_key=nvnugr54fmcvtupxswfcv7yq")
    fun getPlayer(@Path("idPlayer") idPlayer: String): Call<PlayerDTO>
}